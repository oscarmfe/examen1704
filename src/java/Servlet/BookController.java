/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import static java.lang.Long.parseLong;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import model.Book;
import persistence.BookDAO;

/**
 *
 * @author usuario
 */
public class BookController extends BaseController {
    
    private static final int ELEMENTS_PER_PAGE = 2;
    private static final Logger LOG = Logger.getLogger(BookController.class.getName());
    private BookDAO bookDAO;

    public void index(String page) {
        
        int pageNum = 10;
        //objeto persistencia
        bookDAO = new BookDAO();
        ArrayList<Book> books = null;
        
        pageNum = Integer.parseInt(page);
         
        //leer datos de la persistencia
        synchronized (bookDAO) {
            books = BookDAO.getAll(pageNum, ELEMENTS_PER_PAGE);
        } //para que no haya 2 conexiones a la vez
        request.setAttribute("books", books);
        String name = "index";
        LOG.info("En Index de BookController");
        dispatch("/WEB-INF/view/book/index.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/book/create.jsp");
    }
    
    public void insert() throws IOException {

        //objeto persistencia
        bookDAO = new BookDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Book book = loadFromRequest();
        
        synchronized (bookDAO) {
            bookDAO.insert(book);
        }
        redirect(contextPath + "/book/index/1");
    }
    
    public void edit(String idString) throws SQLException {
        LOG.info(""+idString);
        long id = toId(idString);
        BookDAO  bookDAO = new BookDAO();
        Book book = bookDAO.get(id);
        LOG.info("probando");
        request.setAttribute("book", book);
        dispatch("/WEB-INF/view/book/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        BookDAO  bookDAO = new BookDAO();
        LOG.info("en update");
        Book book = loadFromRequest(); 
        bookDAO.update(book);
        response.sendRedirect(contextPath + "/book/index/1");
        return;
    }
    
    public void delete(String idString) throws IOException, SQLException 
    {
        LOG.info("BORRANDO " + idString);
        long id = toId(idString);
        
        BookDAO  bookDAO = new BookDAO();
        bookDAO.delete(id);
        redirect(contextPath + "/book/index/1");
        
    }
 
    
    private Book loadFromRequest()
    {
        Book book = new Book();
        LOG.info("Crear modelo");
        


        book.setId(toId(request.getParameter("id")));
        book.setUser(request.getParameter("user"));
        book.setType(request.getParameter("type"));
        book.setName(request.getParameter("name"));
        book.setDescription(request.getParameter("description"));
        
        LOG.info("Datos cargados");
        return book;
    }
    
     
    public void guardar(String userName) throws SQLException {

        HttpSession session = request.getSession(true);
        session.setAttribute("name", userName);

        redirect(contextPath + "/book/index/1");

    }
    public void seek() throws SQLException {
        dispatch("/WEB-INF/view/book/seek.jsp");
    }
    
     public void found() throws SQLException {
        bookDAO = new BookDAO();
        Book book = new Book();
        long id;
        
        id = parseLong(request.getParameter("numero"));
        synchronized (bookDAO) {
            book = bookDAO.get(id);
        } 
        request.setAttribute("book", book);
        dispatch("/WEB-INF/view/book/details.jsp");

    }
}
