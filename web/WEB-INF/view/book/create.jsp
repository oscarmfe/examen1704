<%@page import="model.Book"%>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Alta de libro </h1>
    <form action="<%= request.getContextPath()%>/book/insert" 
          method="post">
        
        <label>User</label><input name="user" type="text"><br>
        <label>Tipo</label><input name="type" type="text"><br>
        <label>Nombre</label><input name="name" type="text"><br>
        <label>Descripcion</label><input name="description" type="text"><br>        
        
        <input value="Enviar" type="submit"><br>
    </form>     
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
