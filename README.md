# Examen 1704. Examen final Java EE. Marzo/2017

## Base de datos

- Usa el repositorio git@bitbucket.org:rafacabeza/examen1704.git como base del examen.
- Haz un FORK, y después clona el repositorio creado.
- Ejecuta el sql que encontrarás en el repositorio. Este sql crea una base de datos llamada examen1704.
- Todo el examen versa sobre la tabla books

## Ejercicios

1. Ruta '/book/index'. Lista de libros con las columnas: id, titulo, paginas. 2Puntos 
2. Pagina la anterior vista. 2 Puntos. 
3. Crea una vista con el detalle de un libro. Ruta '/book/view/{id}'. 2Puntos. 
4. Crea un formulario de alta de libros y su procesamiento. Rutas '/book/create' y '/book/store'. 2 Puntos. 
5. Añade un enlace en la lista de libros que indique “reservar”. Añade la ruta '/reservation' donde se muestre la lista de libros reservados.